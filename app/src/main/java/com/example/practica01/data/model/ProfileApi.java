package com.example.practica01.data.model;

//import com.chikeandroid.retrofittutorial2.data.model.Post;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ProfileApi {

    @POST("/posts")
    @FormUrlEncoded
    Call<Post> savePost(@Body Post post);
}